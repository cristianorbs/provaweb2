/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package if6ae.bean;

import if6ae.entity.Inscricao;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author cristianopc
 */
@ManagedBean(eager=true)
@ApplicationScoped
public class InscricaoBean {
    private List<Inscricao> inscricao;

    public List<Inscricao> getInscricao() {
        if(inscricao==null){
            inscricao = new ArrayList<Inscricao>();
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("utfpr.ct.dainf.if6ae.avaliacao_avaliacao2-2015-1_war_1.0.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return q.getResultList();
    }

    public void setInscricao(List<Inscricao> inscricao) {
        this.inscricao = inscricao;
    }

    
 }
