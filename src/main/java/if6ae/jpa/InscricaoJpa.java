/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package if6ae.jpa;

import if6ae.entity.Inscricao;
import if6ae.entity.Inscricao_;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author cristianopc
 */
public class InscricaoJpa {
    public Inscricao findByNumero(Integer numInscricao){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("utfpr.ct.dainf.if6ae.avaliacao_avaliacao2-2015-1_war_1.0.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        Root<Inscricao> rt = cq.from(Inscricao.class);
        cq.where(cb.equal(rt.get(Inscricao_.numero), numInscricao));
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return q.getSingleResult();
    }
    public Inscricao findByCpf(long cpf){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("utfpr.ct.dainf.if6ae.avaliacao_avaliacao2-2015-1_war_1.0.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        Root<Inscricao> rt = cq.from(Inscricao.class);
        cq.where(cb.equal(rt.get(Inscricao_.cpf), cpf));
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return q.getSingleResult();
    }
}
