/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package if6ae.jpa;

import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.InscricaoMinicurso_;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author cristianopc
 */
public class InscricaoMinicursoJpa {
    public InscricaoMinicurso findInscricaoMinicursoByNumero(Integer numero){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("utfpr.ct.dainf.if6ae.avaliacao_avaliacao2-2015-1_war_1.0.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InscricaoMinicurso> cq = cb.createQuery(InscricaoMinicurso.class);
        Root<InscricaoMinicurso> rt = cq.from(InscricaoMinicurso.class);
        cq.where(cb.equal(rt.get(InscricaoMinicurso_.inscricao), numero));
        TypedQuery<InscricaoMinicurso> q = em.createQuery(cq);
        return q.getSingleResult();
    }
}
